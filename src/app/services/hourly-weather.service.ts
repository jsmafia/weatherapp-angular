import { Injectable } from '@angular/core';
import { CoordinatesService } from '../services/coordinates.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Weather } from '../interfaces/weather';
import { WeatherComponent } from '../components/weather/weather.component';
import { CurrentDateService } from '../services/current-date.service';
import { WeatherIconService } from '../services/weather-icon.service';
@Injectable({
  providedIn: 'root'
})
export class HourlyWeatherService {
  constructor(
    private currentDateService: CurrentDateService,
    private coordinatesService: CoordinatesService,
    private weatherIconService: WeatherIconService,
    private http: HttpClient
  ) {
    this.onInit();
  }

  completeWeatherArray: Object[][];
  currentWeatherArray: Object[];

  getUrl(lat: number, lng: number): Observable<any> {
    return this.http.get(
      `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&appid=bb5707b9a02b5c08635c421eed9e2690`
    );
  }

  getForecast() {
    return this.coordinatesService.coordinates$.pipe(
      mergeMap(param => this.getUrl(param.lat, param.lng))
    );
  }

  onInit() {
    this.coordinatesService.coordinates$
      .pipe(mergeMap(param => this.getUrl(param.lat, param.lng)))
      .subscribe(data => {
        console.log(data);
        // let requestedData: {} = data;
        this.completeWeatherArray = [];
        this.currentWeatherArray = [];
        let weatherList = data.list;
        let dateString: string = null;
        let index: number = -1;
        let weatherObject: Weather;

        for (let weather of weatherList) {
          let date = weather.dt_txt.split(' ')[0].split('-');
          let day = date[2][0] === '0' ? date[2][1] : date[2];
          let month = date[1][0] === '0' ? date[1][1] : date[1];
          let weatherDescription = weather.weather[0].main;

          let monthName = this.currentDateService.numberToMonth(
            parseInt(month) - 1
          );
          let year = date[0];

          let hour = weather.dt_txt
            .split(' ')[1]
            .split(':')
            .slice(0, 2)
            .join(':');
          let iconUrl = this.weatherIconService.getIconUrl(
            hour.split(':')[0],
            weatherDescription
          );
          let temp = Math.round(weather.main.temp - 273.15);

          weatherObject = {
            hour: hour,
            day: day,
            month: month,
            monthName: monthName,
            year: year,
            temp: temp,
            weatherDescription: weatherDescription,
            iconUrl: iconUrl
          };

          if (weather.dt_txt.split(' ')[0] != dateString) {
            index++;
            dateString = weather.dt_txt.split(' ')[0];
            this.completeWeatherArray[index] = [];
          }
          this.completeWeatherArray[index].push(weatherObject);
          if (index === 0) this.currentWeatherArray.push(weatherObject);
        }
        console.log(this.completeWeatherArray);
      });
  }
}
