import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherIconService {
  constructor() {}

  getIconUrl(hour: number, weather: string): string {
    return `../../../assets/img/weather-icons/${this.chooseNightOrDay(
      hour
    )}/${weather}.png`;
  }

  chooseNightOrDay(hour: number): string {
    // let hour = new Date().getHours();
    if (hour < 20 && hour > 6) {
      return 'day';
    } else {
      return 'night';
    }
  }

  get() {
    return 'haha';
  }
}
