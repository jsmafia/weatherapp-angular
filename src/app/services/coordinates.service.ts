import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoordinatesService {
  latitude: string;
  longitude: string;
  coordinates$: Observable<any>;

  constructor() {
    this.ngOnInit();
  }
  getPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        resp => {
          resolve({
            lng: resp.coords.longitude,
            lat: resp.coords.latitude
          });
        },
        err => {
          reject(err);
        }
      );
    });
  }

  ngOnInit() {
    this.coordinates$ = from(this.getPosition());
  }
}




