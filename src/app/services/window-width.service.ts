import { Injectable } from '@angular/core';
import { SectionActivationService } from './section-activation.service';
@Injectable({
  providedIn: 'root'
})
export class WindowWidthService {
  windowWidth: number;
  isMobile: boolean;

  constructor( private sectionActivationService: SectionActivationService) {}

  isMobileWidth(): void {
    this.isMobile = this.windowWidth <= 600 ? true : false;
    this.sectionActivationService.map = false;
    this.sectionActivationService.calendar = false;
  }

  getWidth(): number {
    return this.windowWidth;
  }

  setWidth(width: number): void {
    this.windowWidth = width;
    this.isMobileWidth();
  }
}
