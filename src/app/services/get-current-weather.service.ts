import { Injectable } from '@angular/core';
import { CoordinatesService } from '../services/coordinates.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetCurrentWeatherService {
  constructor(
    private coordinatesService: CoordinatesService,
    private http: HttpClient
  ) {}

  getCurrentUrl(lat: number, lng: number) {
    return this.http.get(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=bb5707b9a02b5c08635c421eed9e2690`
    );
  }

  getCurrentWeather() {
    return this.coordinatesService.coordinates$.pipe(
      mergeMap(param => this.getCurrentUrl(param.lat, param.lng))
    );
  }
}
