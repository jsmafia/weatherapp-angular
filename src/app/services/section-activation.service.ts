import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SectionActivationService {
  calendar: boolean;
  map: boolean;

  constructor() {
    this.ngOnInit();
  }

  ngOnInit() : void {
    calendar: false;
    map: false;
  }
}
