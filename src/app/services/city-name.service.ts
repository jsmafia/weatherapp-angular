import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root',
})
export class CityNameService {
  
  getCity(latitude: number, longitude: number) {
    return fetch(
      `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&zoom=10&addressdetails=1`
    )
      .then(response => response.json())
      .then(data => data.address.city)
      .catch(error => {
        console.log(error);
        return error;
      });
  }
}
