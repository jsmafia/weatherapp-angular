import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CurrentDateService {
  getCurrentDate() {
    return {
      year: new Date().getFullYear(),
      month: new Date().getMonth(),
      monthName: this.numberToMonth(new Date().getMonth()),
      monthDescription: this.numberToMonthDescription(new Date().getMonth()),
      currentDay: new Date().getDate(),
      dayName: this.numberToDay(new Date().getDay()),
      currentDate: Date.now(),
      currentDateString: this.getDateString()
    };
  }

  getDateString() {
    let date = new Date();
    let year = date.getFullYear();
    let month =
      date.getMonth() + 1 < 10
        ? `0${date.getMonth() + 1}`
        : date.getMonth() + 1;
    let day = date.getDate() < 10 ? `0date.getDate()` : date.getDate();
    return `${year}-${month}-${day}`;
  }

  numberToMonth(month) {
    switch (month) {
      case 0:
        return 'styczeń';
      case 1:
        return 'luty';
      case 2:
        return 'marzec';
      case 3:
        return 'kwiecień';
      case 4:
        return 'maj';
      case 5:
        return 'czerwiec';
      case 6:
        return 'lipiec';
      case 7:
        return 'sierpień';
      case 8:
        return 'wrzesień';
      case 9:
        return 'pażdziernik';
      case 10:
        return 'listopad';
      case 11:
        return 'grudzień';
    }
  }

  numberToMonthDescription(month) {
    switch (month) {
      case 0:
        return 'Stycznia';
      case 1:
        return 'Lutego';
      case 2:
        return 'Marca';
      case 3:
        return 'Kwietnia';
      case 4:
        return 'Maja';
      case 5:
        return 'Czerwca';
      case 6:
        return 'Lipca';
      case 7:
        return 'Sierpnia';
      case 8:
        return 'Września';
      case 9:
        return 'Pażdziernika';
      case 10:
        return 'Listopada';
      case 11:
        return 'Grudnia';
    }
  }

  numberToDay(day) {
    switch (day) {
      case 1:
        return 'Poniedziałek';
      case 2:
        return 'Wtorek';
      case 3:
        return 'Środa';
      case 4:
        return 'Czwartek';
      case 5:
        return 'Piątek';
      case 6:
        return 'Sobota';
      case 0:
        return 'Niedziela';
    }
  }
}
