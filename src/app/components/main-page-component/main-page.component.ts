import { Component, OnInit } from '@angular/core';
import { WindowWidthService } from './../../services/window-width.service';
import { SectionActivationService } from '../../services/section-activation.service';
@Component({
  selector: 'app-main-page-component',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  constructor(
    private windowWidthService: WindowWidthService,
    public sectionActivationService: SectionActivationService
  ) {}

  ngOnInit() {
    this.windowWidthService.setWidth(window.innerWidth);
  }

  onResize(event) {
    this.windowWidthService.setWidth(event.target.innerWidth);
  }
}
