import { Component, OnInit } from '@angular/core';
import { WindowWidthService } from '../../services/window-width.service';
import { SectionActivationService } from '../../services/section-activation.service';
@Component({
  selector: 'app-calendar-container',
  templateUrl: './calendar-container.component.html',
  styleUrls: ['./calendar-container.component.css']
})
export class CalendarContainerComponent implements OnInit {
  constructor(
    public windowWidthService: WindowWidthService,
    private sectionActivationService: SectionActivationService
  ) {}

  ngOnInit() {}

  handleOnClick() {
    this.sectionActivationService.calendar = false;
  }
}
