import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { CurrentDateService } from '../../services/current-date.service';
import { GetCurrentWeatherService } from '../../services/get-current-weather.service';
import { SectionActivationService } from '../../services/section-activation.service';
import { WindowWidthService } from '../../services/window-width.service';
import { WeatherIconService } from '../../services/weather-icon.service';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, AfterViewInit {
  month: number;
  year: number;
  monthName: string;
  firstDayOfMonth: number;
  daysInMonth: number;

  months: string[];
  years: number[];

  daysAbbreviation: string[];
  calendarArray: number[][];

  currentDay: number;
  currentMonth: number;
  currentYear: number;

  currentWeather: object | any;
  weatherDescription: string;
  currentWeatherIcon: string;

  startX: number;
  startY: number;
  distance: number;
  touchObject: object | any;

  clickedCellTopPosition: string;
  clickedCellLeftPosition: string;
  displayDetailsInline: boolean = false;

  constructor(
    private currentDate: CurrentDateService,
    private getCurrentWeatherService: GetCurrentWeatherService,
    private sectionActivationService: SectionActivationService,
    private windowWidthService: WindowWidthService,
    private weatherIconService: WeatherIconService
  ) {}

  ngOnInit() {
    this.getDate();
    this.setArray();
    this.getDays();
    this.createCalendar();
    this.updateCurrentWeatherIcon();
  }
  ngAfterViewInit() {
    console.log('after view');
  }

  getDate(): void {
    ({
      month: this.month,
      year: this.year,
      monthName: this.monthName,
      currentDay: this.currentDay
    } = this.currentDate.getCurrentDate());
    this.currentMonth = this.month;
    this.currentYear = this.year;
  }

  setArray() {
    this.months = [
      'styczeń',
      'luty',
      'marzec',
      'kwiecień',
      'maj',
      'czerwiec',
      'lipiec',
      'sierpień',
      'wrzesień',
      'październik',
      'listopad',
      'grudzień'
    ];
    this.years = [2015, 2016, 2017, 2018, 2019, 2020, 2021];
    this.daysAbbreviation = ['pn', 'wt', 'śr', 'cz', 'pt', 'sb', 'nd'];
  }

  handleMonthChange(value: string): void {
    let monthIndex: number;
    monthIndex = this.months.findIndex(el => {
      if (el === value) return true;
    });
    this.month = monthIndex;
    this.getDays();
    this.createCalendar();
  }

  handleYearChange(value: number): void {
    this.year = value;
    this.getDays();
    this.createCalendar();
  }

  handleClick(direction): void {
    let monthIndex: number = this.months.findIndex(el => {
      if (el === this.monthName) return true;
    });
    let yearIndex: number;

    if (direction === 'left') {
      if (this.months[monthIndex - 1] !== undefined) {
        this.monthName = this.months[monthIndex - 1];
        --this.month;
      } else {
        this.monthName = this.months[this.months.length - 1];
        this.month = 11;
        yearIndex = this.years.findIndex(el => {
          if (el === this.year) return true;
        });

        if (this.years[yearIndex - 1] !== undefined) {
          this.year = this.years[yearIndex - 1];
        } else {
          this.year = this.years[this.years.length - 1];
        }
      }
    }
    if (direction === 'right') {
      if (this.months[monthIndex + 1] !== undefined) {
        this.monthName = this.months[monthIndex + 1];
        ++this.month;
      } else {
        this.monthName = this.months[0];
        this.month = 0;

        yearIndex = this.years.findIndex(el => {
          if (el === this.year) return true;
        });

        if (this.years[yearIndex + 1] !== undefined) {
          this.year = this.years[yearIndex + 1];
        } else {
          this.year = this.years[0];
        }
      }
    }
    this.getDays();
    this.createCalendar();
  }

  getDays(): void {
    let date: Date;

    date = new Date(this.year, this.month);

    this.firstDayOfMonth = date.getDay();
    this.firstDayOfMonth = this.firstDayOfMonth == 0 ? 7 : this.firstDayOfMonth;
    this.daysInMonth = new Date(this.year, this.month + 1, 0).getDate();
  }

  createCalendar(): void {
    let firstDay: number = this.firstDayOfMonth;
    let dayNumber: number = 1;
    let rowIndex: number = 0;

    this.calendarArray = new Array();

    while (dayNumber <= this.daysInMonth) {
      this.calendarArray[rowIndex] = new Array();
      for (let j: number = 1; j <= 7; j++) {
        if (j < firstDay && rowIndex === 0) {
          this.calendarArray[rowIndex][j - 1] = null;
        } else {
          if (dayNumber <= this.daysInMonth) {
            this.calendarArray[rowIndex][j - 1] = dayNumber;
          } else {
            this.calendarArray[rowIndex][j - 1] = null;
          }

          dayNumber++;
        }
      }
      rowIndex++;
    }
  }

  isCurrent(rowIndex: number, dayIndex: number): boolean {
    if (
      this.calendarArray[rowIndex][dayIndex] === this.currentDay &&
      this.currentMonth === this.month &&
      this.currentYear == this.year
    ) {
      return true;
    } else {
      return false;
    }
  }

  isEmpty(rowIndex: number, dayIndex: number): boolean {
    if (this.calendarArray[rowIndex][dayIndex] === null) {
      return true;
    }
  }

  updateCurrentWeatherIcon() {
    this.getCurrentWeatherService.getCurrentWeather().subscribe(data => {
      this.currentWeather = data;
      this.weatherDescription = this.currentWeather.weather[0].main;
      this.currentWeatherIcon = this.weatherIconService.getIconUrl(
        new Date().getHours(),
        this.weatherDescription
      );
    });
  }

  onWidgetClickEvent() {
    this.sectionActivationService.calendar = false;
  }

  onTouchStartEvent(event: TouchEvent) {
    this.touchObject = event.changedTouches[0];
    this.startX = this.touchObject.pageX;
    this.startY = this.touchObject.pageY;
  }
  onTouchEndEvent(event: TouchEvent) {
    this.touchObject = event.changedTouches[0];
    this.distance = this.touchObject.pageX - this.startX;
    if (this.distance < -100) {
      this.onWidgetClickEvent();
    }
  }
  onClickMe(event: any) {
    this.displayDetailsInline = true;
    console.log(event.path[0].innerText);
    // event.preventDefault();
    // event.stopPropagation();
  }
  onHideDetails(event: any) {
    if (event.target === event.currentTarget.firstChild){
      this.displayDetailsInline = false;
    }
  }
}
