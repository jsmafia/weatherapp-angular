import { Component, OnInit } from '@angular/core';
import { WindowWidthService } from '../../services/window-width.service';
import { SectionActivationService } from '../../services/section-activation.service';
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  startX: number;
  startY: number;
  distance: number;
  touchObject: object | any;

  constructor(
    public windowWidthService: WindowWidthService,
    public sectionActivationService: SectionActivationService
  ) {}

  ngOnInit() {}

  handleOnClick() {
    this.sectionActivationService.map = false;
  }
  onTouchStartEvent(event: TouchEvent) {
    if (this.windowWidthService.isMobile) {
    this.touchObject = event.changedTouches[0];
    this.startX = this.touchObject.pageX;
    this.startY = this.touchObject.pageY;
  }
}
  onTouchEndEvent(event: TouchEvent) {
    this.touchObject = event.changedTouches[0];
    this.distance = this.touchObject.pageX - this.startX;
    if (this.distance > 150) {
      this.handleOnClick();
    }
  }
}

