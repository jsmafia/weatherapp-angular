import { Component, OnInit } from '@angular/core';
import { CityNameService } from '../../../services/city-name.service';
import { CoordinatesService } from '../../../services/coordinates.service';

@Component({
  selector: 'app-city-name',
  templateUrl: './city-name.component.html',
  styleUrls: ['./city-name.component.css'],
})
export class CityNameComponent implements OnInit {
  
  currentCity: string;

  constructor(private _cityNameService: CityNameService, private _coordinatesService: CoordinatesService) {}

  ngOnInit() {
    
    this._coordinatesService
      .getPosition()
      .then(({ lat,lng  }) => this._cityNameService.getCity(lat,lng ))
      .then(city => (this.currentCity = city));
  }
}
