import { Component, OnInit } from '@angular/core';
import { CoordinatesService } from '../../../services/coordinates.service';
declare let L;

@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.css']
})
export class LeafletMapComponent implements OnInit {
  longitude: number = null;
  latitude: number = null;

  constructor(private coordinatesService: CoordinatesService) {}

  ngOnInit() {
    this.coordinatesService.getPosition().then(pos => {
      this.latitude = pos.lat;
      this.longitude = pos.lng;
      const map = L.map('map-container').setView(
        [this.latitude, this.longitude],
        12
      );
      const sunnyIcon = L.icon({
        iconUrl: '../../../../assets/img/location.png',
        iconSize: [48, 48],
        iconAnchor: [22, 94]
      });
      L.marker([this.latitude, this.longitude], {
        icon: sunnyIcon
      }).addTo(map);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
          '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);
    });
  }
}

