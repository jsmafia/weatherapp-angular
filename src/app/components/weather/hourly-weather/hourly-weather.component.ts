import { Component, OnInit } from '@angular/core';
import { HourlyWeatherService } from '../../../services/hourly-weather.service';
import { CurrentDateService } from '../../../services/current-date.service';
@Component({
  selector: 'app-hourly-weather',
  templateUrl: './hourly-weather.component.html',
  styleUrls: ['./hourly-weather.component.css']
})
export class HourlyWeatherComponent implements OnInit {
  weatherObject: Object | any;
  weatherList: Object[];
  filteredList: Array<any>;
  simplifiedList: Object[];

  constructor(
    private hourlyWeatherService: HourlyWeatherService,
    private currentDateService: CurrentDateService
  ) {}

  ngOnInit() {}
}
