import { Component, OnInit } from '@angular/core';
import { GetCurrentWeatherService } from '../../../services/get-current-weather.service';
import { CurrentDateService } from '../../../services/current-date.service';
import { WeatherIconService } from '../../../services/weather-icon.service';
@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {
  currentWeather: Object | any;
  currentTemperature: number;
  weatherDescription: string;
  polishWeatherDescription: string;
  currentWeatherIcon: string;
  fullDate: string;

  constructor(
    private getCurrentWeatherService: GetCurrentWeatherService,
    private currentDateService: CurrentDateService,
    private weatherIconService: WeatherIconService
  ) {}

  ngOnInit() {
    this.launchCurrentWeather();
    this.setFullDate();
  }

  setFullDate() {
    let {
      dayName,
      currentDay,
      monthDescription,
      year
    } = this.currentDateService.getCurrentDate();
    this.fullDate = `${dayName}, ${currentDay} ${monthDescription} ${year}`;
  }

  launchCurrentWeather() {
    this.getCurrentWeatherService.getCurrentWeather().subscribe(data => {
      console.log(data);
      this.currentWeather = data;
      this.currentTemperature = Math.round(
        this.currentWeather.main.temp - 273.15
      );
      this.weatherDescription = this.currentWeather.weather[0].main;
      this.updateActualWeather(this.weatherDescription);
      //obiekt currentWeather
    });
  }

  translateWeather(weatherDescription) {
    switch (weatherDescription) {
      case 'Thunderstorm':
        return 'Burza';
      case 'Drizzle':
        return 'Mżawka';
      case 'Rain':
        return 'Opady deszczu';
      case 'Snow':
        return 'Opady śniegu';
      case 'Clear':
        return 'Czyste niebo';
      case 'Clouds':
        return 'Zachmurzenie';
      case 'Mist':
        return 'Mgła';
    }
  }

  updateActualWeather(weatherDescription) {
    this.polishWeatherDescription = this.translateWeather(
      this.weatherDescription
    );
    this.currentWeatherIcon = this.weatherIconService.getIconUrl(
      new Date().getHours(),
      this.weatherDescription
    );
  }
}
