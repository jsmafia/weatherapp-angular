import { Component, OnInit } from '@angular/core';
import { WindowWidthService } from '../../services/window-width.service';
import { UserNameService } from '../../services/user-name.service';
import { SectionActivationService } from '../../services/section-activation.service';
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  startX: number;
  startY: number;
  distance: number;
  touchObject: object | any;

  constructor(
    public windowWidthService: WindowWidthService,
    private userNameService: UserNameService,
    private sectionActivationService: SectionActivationService
  ) {}

  Name = this.userNameService.isNewUser();

  ngOnInit() {}

  onCalendarClickEvent() {
    this.sectionActivationService.calendar = true;
  }

  onMapClickEvent() {
    this.sectionActivationService.map = true;
  }
  onWidgetClickEvent() {
    this.sectionActivationService.map = false;
  }
  onTouchStartEvent(event: TouchEvent) {
    this.touchObject = event.changedTouches[0];
    this.startX = this.touchObject.pageX;
    this.startY = this.touchObject.pageY;
}
  onTouchEndEvent(event: TouchEvent) {
    this.touchObject = event.changedTouches[0];
    this.distance = this.touchObject.pageX - this.startX;
    if (this.distance > 100) {
      this.onCalendarClickEvent();
      this.onWidgetClickEvent();
    }
    if ( this.distance < -100) {
      this.onMapClickEvent();
    }
  }


}
