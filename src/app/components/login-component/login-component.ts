import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.html',
  styleUrls: ['./login-component.css']
})
export class LoginComponent implements OnInit {

  userName=""

  constructor(private router: Router) {

   }

  ngOnInit() {
  }

  //zapisywanie usera do localstorage
  saveUserName(name) {
      localStorage.setItem("username", name);
    }



  onAddName(form: NgForm) {
    //nazwa użytkownika zgodna z wpisaną w input
    this.userName = form.value.name;
    //zapisuje nazwę użytkownika do localStorage
    this.saveUserName(this.userName);
    this.router.navigate(['/mainWindow']);

  }
  }





