import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
// Components
import { AppComponent } from './app.component';
import { CalendarContainerComponent } from './components/calendar/calendar-container.component';
import { WeatherComponent } from './components/weather/weather.component';
import { LocationComponent } from './components/location/location.component';
import { LeafletMapComponent } from './components/location/leaflet-map/leaflet-map.component';
import { CityNameComponent } from './components/location/city-name/city-name.component';
import { LocationContainerComponent } from './components/location/location-container/location-container.component';
import { LoginComponent } from './components/login-component/login-component';
import { MainPageComponent} from './components/main-page-component/main-page.component';


import { CalendarComponent } from './components/calendar/calendar.component';
import { HourlyWeatherComponent } from './components/weather/hourly-weather/hourly-weather.component';
import { CurrentWeatherComponent } from './components/weather/current-weather/current-weather.component';

// Services
import { ServiceExampleService } from './services/service-example.service';
import { PopUpDetailsComponent } from './components/calendar/pop-up-details/pop-up-details.component';
// Thanks to @Injectable decorator, we don't have to import each service in here - they are passed straight to root component


const appRoutes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'mainWindow', component: MainPageComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    CalendarContainerComponent,
    WeatherComponent,
    LocationComponent,
    LeafletMapComponent,
    CityNameComponent,
    LocationContainerComponent,
    CalendarComponent,
    HourlyWeatherComponent,
    CurrentWeatherComponent,
    LoginComponent,
    MainPageComponent,
    PopUpDetailsComponent,
  ],


  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    LeafletModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],

  providers: [ServiceExampleService],

  bootstrap: [AppComponent]
})
export class AppModule {}
