export interface Weather {
  hour: string;
  day: number;
  month: number;
  monthName: string;
  year: number;
  temp: number;
  weatherDescription: string;
  iconUrl: string;
}
