import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weatherApp';

  constructor() {}

  //   ngOnInit() {
  //     this.windowWidthService.setWidth(window.innerWidth);
  //   }

  //   onResize(event) {
  //     this.windowWidthService.setWidth(event.target.innerWidth);
  //   }
}
